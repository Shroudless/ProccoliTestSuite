** Settings ***
Documentation
Resource       resources.robot
Library        Selenium2Library
*** Variables ***
${Page}     https://enna.d34z0o9pzl90xp.amplifyapp.com/login

***Test Cases***
Login Test
    Set Selenium Speed    0.3 seconds
    Open Browser    ${Page}     chrome    
    Maximize Browser Window
    Login Manager
    Check Account

New Request Test
    Fill Up Request Details
    Select RFQ Date
    Fill Up Requesting Items
    Submit RFQ

RFT/RFQ Page Test
    Click RFT/RFQ Page
    Cycle Tabs
    Check First RFQ
    Click Received Quotes  #page doesn't load for some reason
    Click Notifications
    Click Live Quotes
    Delete First RFQ
    #Delete RFQ  #ensure no state change for fresh test
    #Click First Notification
    #Create Reply

Supplier Page Test
    Click Supplier Page
    Add New Supplier
    Fill Up New Supplier Form

Sign Out Test
    Sign Out
    Check Front Page




