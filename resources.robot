*** Variables ***
${Email}     css:#normal_login_email
${Password}    css:#normal_login_password
${Button}    css:.ant-btn  #several butttons use this, so this keyword will be reused
${Profile Details}    css:.ant-typography
${Dummy Account}    Liu Guanzhong
${Settings Button Tab}    css:.ant-menu-item-selected
${Settings Page}    css:h3.ant-typography
${Projects}    css:.ant-checkbox-group > label:nth-child(1) > .ant-checkbox
${Tender Package Deadline}    css:.ant-checkbox-group > label:nth-child(2) > .ant-checkbox
${Tender Packages}    css:.ant-checkbox-group > label:nth-child(3) > .ant-checkbox
${Bid Submissions}    css:.ant-checkbox-group > label:nth-child(4) > .ant-checkbox
${First Name}    css:#firstName
${Last Name}    css:#lastName
${Company Name}    css:#name
${UEN}    css:#uen
${Postal Code}    css:#postalCode
${Contact Number}    css:#contactNumber
${Website}    css:#website
${Manager Tab}    css:#tab-1
${New Request Button}    css:.ant-btn-primary
${Loading Spinner}    css:.ant-spin-dot.ant-spin-dot-spin  #fuck finding this shit >.<
${New Request Title}    css:#title
${New Request Description}    css:#description
${New Request Address}    css:#address
${Categories}    css:.ant-select-multiple
${Category Security}    css:.ant-select-item-option-content
${Requester Name}    css:#nameOfRequester
${Request Type}    css:#requestType
${Request Type Single}    css:#requestType
${Invitation Option}    css:label.ant-radio-wrapper:nth-child(2) > span:nth-child(1) > input:nth-child(1)
${RFQ Closing Date}    css:.ant-picker-input
${Picked Date}        css:.ant-picker-datetime-panel > div:nth-child(1) > div:nth-child(2) > table:nth-child(1) > tbody:nth-child(2) > tr:nth-child(5) > td:nth-child(6) > div
${Calendar OK Button}    css:.ant-btn-primary.ant-btn-sm
${Requested Service}    css:.ant-table-row > .ant-table-cell:nth-child(1) > .editable-cell-value-wrap
${Requested Service Input}    xpath://*[@id="item"]
${Requested Unit}    css:.ant-table-row > .ant-table-cell:nth-child(2) > .editable-cell-value-wrap
${Requested Unit Input}    xpath://*[@id="unit"]
${Requested Unit of Measure}    css:.ant-table-row > .ant-table-cell:nth-child(3) > .editable-cell-value-wrap
${Requested Unit of Measure Input}    xpath://*[@id="unitOfMeasure"]
${Requested Remarks}    css:.ant-table-row > .ant-table-cell:nth-child(4) > .editable-cell-value-wrap
${Requested Remarks Input}    xpath://*[@id="remarks"]
${Request Next Button}    css:.ant-btn-primary.ant-btn-round ~ .ant-btn
${Preview Button}    css:button.ant-btn:nth-child(3)
#${Latest Delivery Date}    css:div.ant-col-lg-8:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div
#${Delivery Picked Date}    css:body > div:nth-child(8) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > table:nth-child(1) > tbody:nth-child(2) > tr:nth-child(5) > td:nth-child(3) > div:nth-child(1)
#${Expected Delivery Date}    css:div.ant-row:nth-child(18) > div:nth-child(4) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)
#${Expected Picked Date}    css:td.ant-picker-cell-selected:nth-child(3) > div:nth-child(1)
#${Lead Time}    css:#estimatedLeadTime
${Submit Button}    css:.ant-btn.ant-btn-primary:nth-child(2)
${RFT/RFQ Tab}    css:.ant-menu-item:nth-child(2)
${Suppliers Tab}  css:.ant-menu-item:nth-child(3)
${Tab 1}    css:#tab-1
${Tab 2}    css:#tab-2
${Tab 3}    css:#tab-3
${First RFQ}    css:.ant-btn.ant-btn-link
${First RFQ Delete}    css:.ant-table-cell:nth-child(6) > .ant-btn.ant-btn-link
${First RFQ Delete Confirm}    css:.ant-btn.ant-btn-primary.ant-btn-sm
${First Notification}    css:.ant-table-body > table > .ant-table-tbody > .ant-table-row
${Reply Text Box}    css:.ant-input
${Reply Button}    css:.ant-btn.ant-btn-primary
${Sign Out Button}    css:.ant-btn.ant-btn-round.ant-btn-background-ghost
${Add New Supplier Button}    css:.main-content-wrapper > div:nth-child(4) > button:nth-child(1)
${Company Name Field}    css:#userForm_name
${UEN Field}    css:#userForm_uen
${Address Field}    css:#userForm_address
${Postal Code Field}    css:#userForm_postalCode
${Email Field}    css:#userForm_email
${Contact Number Field}    css:#userForm_contactNumber
${Contact Person Field}    css:#userForm_contactPerson
${Fax Field}    css:#userForm_fax
${Service Category Field}    css:#userForm_category
${Service Category Security}     css:div.ant-select-item:nth-child(1) > div:nth-child(1)


*** Keywords ***
Login Vendor
    Input Text    ${Email}      Karen Kujo
    Input Text    ${Password}    AYAYA
    Click Element    ${Button}

Login Manager
    Click Element    ${Manager Tab}
    Input Text    ${Email}      jinyantan109@gmail.com
    Input Text    ${Password}    test123
    Click Element    ${Button}
    Wait Until Element Is Not Visible    ${Loading Spinner}    30
    #Wait Until Element Is Not Visible    ${Loading Spinner} 

Check Account
    Wait Until Element Is Visible    ${Profile Details}
    Sleep  5 seconds
    Element Text Should Be    ${Profile Details}   Daddy Poh Teng Ban

Check Manager
    Wait Until Element Is Visible    ${Profile Details}
    Element Text Should Be    ${Profile Details}   ${Dummy Account}

Checking Contact Details
    Textfield Value Should Be    ${First Name}    Ayaya
    Textfield Value Should Be    ${Last Name}    Zen

Checking Company Details
    Textfield Value Should Be    ${Company Name}    Proccoli
    Textfield Value Should Be    ${UEN}    A6666666
    Textfield Value Should Be    ${Postal Code}    600600
    Textfield Value Should Be    ${Contact Number}    9666666
    Textfield Value Should Be    ${Website}    https://proccoli.com

Shift To Settings Tab
    Click Element    ${Settings Button Tab} 
    Wait Until Element Is Visible    ${Settings Page}
    Element Text Should Be    ${Settings Page}    Settings

Turn On All Notifications
    Click Element    ${Projects} 
    Click Element    ${Tender Package Deadline}
    Click Element    ${Tender Packages} 
    Click Element    ${Bid Submissions}

Submit RFQ
    Click Element    ${Request Next Button}
    Wait Until Element Is Visible    ${Preview Button}
    Click Element    ${Preview Button}
    Wait Until Element Is Visible    ${Submit Button} 
    Click Element    ${Submit Button} 
    Wait Until Element Is Visible    css:.ant-spin
    Wait Until Element Is Not Visible    css:.ant-spin

Select RFQ Date
    Click Element    ${RFQ Closing Date}
    Click Element    ${Picked Date}
    Click Element    ${Calendar OK Button}

Fill Up Request Details
    Click Element   ${New Request Button}
    Wait Until Element Is Not Visible    ${Loading Spinner}
    Wait Until Element Is Visible    ${New Request Title}
    Input Text    ${New Request Title}    Need more security guards
    Input Text    ${New Request Description}    Too many people breaking in, need more guards
    Input Text    ${New Request Address}     The house on the side of the road
    Click Element    ${Categories} 
    Click Element    ${Category Security}
    Input Text    ${Requester Name}    Karen Kujo
    #Scroll Element Into View    css:.ant-form-item-control-input>.ant-form-item-control-input-content>div>.ant-btn.ant-btn-primary
    #Click Element    ${Request Type}
    #Wait Until Element Is Visible    ${Request Type Single}
    #Click Element at Coordinates    ${Request Type Single}  0  -40  #default value is now single, for some reason not clickable
    #Click Element    ${Invitation Option}

Fill Up Requesting Items
    Click Element    css:.ant-form-item-control-input>.ant-form-item-control-input-content>div>.ant-btn.ant-btn-primary
    Click Element    ${Requested Service}
    #Press Keys    ${Requested Service Input}    \\8\\8\\8\\8\\8\\8\\8\\8\\8\\8\\8\\8\\8\\8\\8\\8
    Wait Until Element Is Visible   ${Requested Service Input} 
    Press Keys    ${Requested Service Input}    More guards
    Click Element    ${Requested Unit}
    #Press Keys    ${Requested Service Input}    BACKSPACE 
    Wait Until Element Is Visible    ${Requested Unit Input}
    Press Keys    ${Requested Unit Input}    20
    Click Element    ${Requested Unit of Measure}
    #Press Keys    ${Requested Service Input}    \\8\\8\\8\\8
    Wait Until Element Is Visible    ${Requested Unit of Measure Input} 
    Press Keys    ${Requested Unit of Measure Input}    People
    Click Element    ${Requested Remarks}
    #Press Keys    ${Requested Service Input}    \\8\\8\\8\\8\\8\\8\\8\\8\\8\\8\\8\\8\\8\\8\\8\\8
    Wait Until Element Is Visible    ${Requested Remarks Input}
    Press Keys    ${Requested Remarks Input}    Make sure they are good

Click RFT/RFQ Page
    Click Element    ${RFT/RFQ Tab} 
    Wait Until Element Is Not Visible    css:.ant-spin

Cycle Tabs
    Wait Until Element Is Visible    ${Tab 2}
    Click Element    ${Tab 2}
    Click Element    ${Tab 3}
    Click Element    ${Tab 1}

Check First RFQ Contents
    Switch Window    NEW
    Wait Until Element Is Not Visible    css:.ant-spin    30
    Element Text Should Be    css:tbody>.ant-descriptions-row:nth-child(2)>.ant-descriptions-item-content:nth-child(1)    Need more security guards
    Element Text Should Be    css:tbody>.ant-descriptions-row:nth-child(2)>.ant-descriptions-item-content:nth-child(2)    Too many people breaking in, need more guards
    Element Text Should Be    css:tbody>.ant-descriptions-row:nth-child(2)>.ant-descriptions-item-content:nth-child(3)    The house on the side of the road
    Element Text Should Be    css:tbody>.ant-descriptions-row:nth-child(4)>.ant-descriptions-item-content:nth-child(1)    Karen Kujo
    Element Text Should Be    css:tbody>.ant-descriptions-row:nth-child(4)>.ant-descriptions-item-content:nth-child(2)    Single
    Element Text Should Be    css:tbody>.ant-descriptions-row:nth-child(4)>.ant-descriptions-item-content:nth-child(3)>.ant-tag    SECURITY
    Element Text Should Be    css:tbody>.ant-descriptions-row:nth-child(6)>.ant-descriptions-item-content:nth-child(1)    RFQ
    Element Text Should Be    css:tbody>.ant-descriptions-row:nth-child(6)>.ant-descriptions-item-content:nth-child(2)    Invitation
    Scroll Element Into View    css:.ant-table-row>.ant-table-cell:nth-child(1)
    Element Text Should Be    css:.ant-table-row>.ant-table-cell:nth-child(1)    More guards
    Element Text Should Be    css:.ant-table-row>.ant-table-cell:nth-child(2)    20
    Element Text Should Be    css:.ant-table-row>.ant-table-cell:nth-child(3)    People
    Element Text Should Be    css:.ant-table-row>.ant-table-cell:nth-child(4)    Make sure they are good
    Close Window

Check First RFQ
    #Wait Until Element Contains    css:.ant-statistic-title    Live RFQ
    Wait Until Element Is Visible    ${First RFQ}
    Click Element    ${First RFQ}
    Wait Until Element Is Not Visible    css:.ant-spin    30
    Check First RFQ Contents
    Switch Window    url:https://enna.d34z0o9pzl90xp.amplifyapp.com/users/requests

Click Live Quotes
    Click Element    ${Tab 1}

Click Received Quotes
    Click Element    ${Tab 2}

Click Notifications
    Click Element    ${Tab 3}

Click First Notification
    Click Element    ${First Notification}

Delete First RFQ
    Click Element    ${First RFQ Delete}    
    Click Element    ${First RFQ Delete Confirm}

Create Reply
    Input Text    ${Reply Text Box}    What does the D C stand for or CD stand for
    Click Element    ${Reply Button}

Sign Out
    Click Element    ${Sign Out Button}

Check Front Page
    Element Text Should Be    css:.ant-typography    Don't have an account yet? Sign Up!

Click Supplier Page
    Click Element    ${Suppliers Tab}

Add New Supplier
    Click Element    ${Add New Supplier Button}

Fill Up New Supplier Form
    Click Element    ${Company Name Field} 
    Press Keys    ${Company Name Field}    Security Company #1
    Click Element    ${UEN Field}
    Press Keys    ${UEN Field}    S123456789
    Click Element    ${Address Field}
    Press Keys    ${Address Field}    12 Something Lane
    Click Element    ${Postal Code Field}
    Press Keys    ${Postal Code Field}    612345
    Click Element    ${Email Field}
    Press Keys    ${Email Field}    omegalul@gmail.com
    Click Element    ${Contact Number Field} 
    Press Keys    ${Contact Number Field}    91234567
    Click Element    ${Contact Person Field}
    Press Keys    ${Contact Person Field}    Karen Kujo
    Click Element    ${Fax Field}
    Press Keys    ${Fax Field}    66666666
    Click Element    ${Service Category Field}
    Click Element    ${Service Category Security}
    Click Element    css:body > div:nth-child(4) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(4) > div:nth-child(1) > button:nth-child(2)

